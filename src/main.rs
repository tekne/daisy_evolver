extern crate rayon;
extern crate csv;
extern crate serde;
extern crate stats;
extern crate rand;

#[macro_use]
extern crate serde_derive;

pub mod optimizer;
pub mod squaresum;

use std::iter::FromIterator;
use std::io;

pub mod cars;
pub mod track;
pub mod simulator;
pub mod evolver;

pub fn main() -> io::Result<()> {
    let mut arguments = std::env::args();
    let name = arguments.next().unwrap();
    println!("Car Evolver called with {}", name);
    let first_arg = arguments.next().unwrap();
    match first_arg.as_ref() {
        "squaresum" => squaresum::squaresum(),
        "evolver" => evolver::evolve(Vec::from_iter(arguments.into_iter())),
        s => {
            println!("Invalid first argument \"{}\"", s);
            Ok(())
        }
    }
}
