use rand::Rng;
use crate::cars::{Car, SimplePathParameters};
use crate::track::{MaxVelocityTrack};
use std::iter::FromIterator;

#[derive(Debug, PartialEq, Clone)]
pub struct Simulator {
    pub car : Car,
    pub param : SimplePathParameters,
    pub history : Vec<Status>,
    pub inst : Vec<Instruction>
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Status {
    pub gas : f64,
    pub tire : f64,
    pub vel : f64,
    pub n : u32
}

impl Status {
    pub fn starting(car : &Car, param : &SimplePathParameters) -> Status {Status{
        gas : car.max_gas(),
        tire : car.max_tire(),
        vel : 0.0,
        n : param.n
    }}
    pub fn pitstop(car : &Car, prev : &Status) -> Status {Status{
        gas : car.max_gas(),
        tire : car.max_tire(),
        vel : 0.0,
        n : prev.n
    }}
    pub fn acc(&self, acc : f64) -> Status {
        let nv = Car::get_acc_nv(self.vel, acc);
        Status {
            gas : if acc > 0.0 {(self.gas - 0.1 * acc * acc).max(0.0)} else {self.gas},
            tire : if acc < 0.0 {self.tire - 0.1 * acc * acc} else {self.tire},
            vel : nv,
            n : self.n
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Instruction { //acceleration values/pit stops over time
    pub acc : f64,
    pub pit : bool
}


impl Simulator {
    pub fn new(car : Car, param : SimplePathParameters, reserve : usize) -> Simulator {
        let mut result = Simulator{
            history : Vec::with_capacity(reserve),
            inst : Vec::with_capacity(reserve),
            car : car,
            param : param,
        };
        result.history.push(Status::starting(&result.car, &result.param));
        result
    }

    pub fn vel(&self) -> f64 {self.history.last().unwrap().vel}
    pub fn n(&self) -> u32 {self.history.last().unwrap().n}
    pub fn tire(&self) -> f64 {self.history.last().unwrap().tire}
    pub fn gas(&self) -> f64 {self.history.last().unwrap().gas}
    pub fn i(&self) -> usize {self.inst.len()}

    pub fn stop_at(&self) -> usize {
        for (i, (v, t)) in self.history.iter()
            .map(|entry| (entry.vel, entry.tire))
            .enumerate().rev() {
            let acc = Car::acc_between(v, 0.0);
            if 0.1 * acc * acc <= t {
                return i;
            }
        }
        unreachable!()
    }

    pub fn get_forward_acc(&mut self, track : &MaxVelocityTrack) -> f64 {
        let end_ind = (self.i() + self.n().max(2) as usize).min(track.points.len());
        let slopes = Vec::from_iter(
            track.points[(self.i() + 1)..end_ind].iter()
            .enumerate().map(|(j, v)| (v - self.vel())/((j + 1) as f64))
        );
        let minimizer = slopes.iter().enumerate()
            .min_by(|&(_, x), &(_, y)| x.partial_cmp(y).unwrap());
        let (_min_index, dv) = minimizer.unwrap();
        let dv = dv.min(self.car.top_speed() - self.vel());

        if dv > 0.00001 || dv < 0.00001 {
            let nv = self.vel() + dv;
            let acc = Car::acc_between(self.vel(), nv)
                .min(self.car.max_acc())
                .max(self.car.max_break());
            //println!("Logging acc = {}, nv = {}, dv = {}", acc, nv, dv);
            acc
        } else {
            0.0
        }
    }

    pub fn simulate<T: Rng>(&mut self, track : &MaxVelocityTrack, rng : &mut T) -> bool {
        let mut breaking = false;
        let mut count : usize = 0;

        for point in &track.points {
            assert!(*point <= self.car.top_speed());
        }

        while self.i() < track.points.len() - 1 {//i is current velocity, v_max is the next

            if count == 0 {
                print!(".");
            }
            if count > 10000 {
                return true;
            }
            count += 1;

            assert!(self.history.len() - self.i() == 1);
            //println!("History @{} - Inst @{}: {:?}", self.history.len(), self.i(), self.history.last().unwrap());

            if self.vel() > track.points[self.i()].min(self.car.top_speed()) {

                //println!("TOO FAST");

                assert!(self.i() < self.history.len());

                let mut num_back_tracks = 0;

                self.history.last_mut().unwrap().n += self.param.n;
                let n = self.n();

                while num_back_tracks < n && self.history.len() > 1 as usize {
                    let prev = self.history.pop().unwrap();
                    //println!("POP : {:?}", prev);
                    self.inst.pop();
                    self.history.last_mut().unwrap().n = prev.n;
                    num_back_tracks += 1;
                }


            } else if (self.tire() < 0.0 || self.gas() < 0.0) && !breaking {

                //println!("TRUNCATE");

                breaking = true;
                self.history.pop();
                self.inst.pop();

                let stop_at = self.stop_at();
                self.history.truncate(stop_at + 1);
                self.inst.truncate(stop_at);
            } else if breaking {

                //println!("BREAKING!");

                let acc = Car::acc_between(self.vel(), 0.0).max(self.car.max_break());

                //TODO: think about this...
                if self.vel() >= -0.000001 && self.vel() <= 0.000001 { // Pit stop
                    //println!("PITSTOP!");
                    let acc = self.get_forward_acc(track);
                    //TODO: add acceleration calculation code!!!
                    self.history.push(Status::pitstop(&self.car, self.history.last().unwrap()));
                    self.inst.push(Instruction{
                        acc : acc, // TODO!!!
                        pit : true
                    });
                    breaking = false;
                    continue;
                }
                self.history.push(self.history.last().unwrap().acc(acc));
                self.inst.push(Instruction{
                    acc : acc,
                    pit : false
                });
            } else {

                //println!("NORMIE!");

                let acc = self.get_forward_acc(track);
                self.history.push(self.history.last().unwrap().acc(acc));
                self.inst.push(Instruction{acc : acc, pit : false});
                // Update lookahead parameters
                if rng.gen_bool(self.param.pa) {
                    self.history.last_mut().unwrap().n = if self.n() < self.param.a
                    {self.param.min_n} else {
                        (self.n() - self.param.a).min(self.param.min_n)
                    };
                }
            }
        }
        self.inst.push(Instruction{acc : 0.0, pit : false});
        false
    }

    pub fn get_time(&self) -> f64 {
        let mut time = 0.0;
        for i in 0..self.history.len() {
            time += if self.inst[i].acc == 0.0 || i == self.history.len() {
                1.0 / self.history[i].vel
            } else {
                (self.history[i + 1].vel - self.history[i].vel)/self.inst[i].acc
            };
            if self.inst[i].pit {time += 30.0;}
        }
        time
    }
}
