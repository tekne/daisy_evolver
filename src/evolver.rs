use crate::optimizer::{Scorer, Optimizer};

use crate::cars::{Car, SimplePathParameters, CarSimulationParams};
use crate::simulator::{Simulator};
use crate::track::{Track, MaxVelocityTrack};

use std::io;
use std::io::Write;
use std::fs::File;

use std::iter::{FromIterator, IntoIterator};

#[derive(Clone, PartialEq, Debug)]
pub struct CarParamScorer {
    weights : Vec<f64>,
    tracks : Vec<Track>
}

impl CarParamScorer {
    pub fn from_csvs(weights : Vec<f64>, csvs : Vec<File>) -> CarParamScorer {
        CarParamScorer {
            weights : weights,
            tracks : Vec::from_iter(csvs.into_iter().map(|f| Track::from_csv(f)))
        }
    }
}

impl Scorer<CarSimulationParams> for CarParamScorer {
    fn loss(&self, specimen : &CarSimulationParams) -> f64 {
        let mut rng = rand::thread_rng();
        let mut velocity_tracks : Vec<MaxVelocityTrack> = self.tracks.iter()
            .map(|t| MaxVelocityTrack{points : t.to_max_vel(&specimen.car)})
            .collect();
        let mut total_time = 0.0;
        for track in &mut velocity_tracks {track.adjust();}
        for (params, (weight, track)) in specimen.params.iter().zip(self.weights.iter().zip(velocity_tracks.iter())) {
            let mut simulator = Simulator::new(specimen.car, params.clone(), track.points.len());
            if simulator.simulate(track, &mut rng) {
                total_time += 3000000.0;
                break;
            }
            total_time += weight * simulator.get_time();
        }
        return total_time;
    }
}

pub fn evolve(args : Vec<String>) -> io::Result<()> {

    println!("Source files ({}): {:?}", args.len(), args);

    let mut rng = rand::thread_rng();
    let learning_rate = 0.05;
    let change_rate = 0.05;
    let n_init = 2;
    let mut init_cars = Vec::with_capacity(n_init);
    for _ in 0..n_init {
        init_cars.push(CarSimulationParams{
            car : Car::new_random(18, &mut rng),
            params : std::iter::repeat(SimplePathParameters::default()).take(args.len()).collect()
        });
    }
    println!("Starting cars:\n{:?}", init_cars);

    let files = Vec::from_iter(args.iter().map(|s| File::open(s).unwrap()));
    let weights = [1.0, 0.25, 0.25, 0.25, 0.5, 0.5, 1.0, 1.0];
    let names = [
        "instruction_1.csv",
        "instruction_2.csv",
        "instruction_3.csv",
        "instruction_4.csv",
        "instruction_5.csv",
        "instruction_6.csv",
        "instruction_7.csv",
        "instruction_8.csv",
    ];
    let mut optimizer = Optimizer::new(
        init_cars,
        CarParamScorer::from_csvs(weights[..files.len()].to_vec(), files)
    );

    let stdin = io::stdin();
    let mut stdout = io::stdout();
    let mut buffer = String::new();
    let mut generation = 0;
    loop {
        print!("{} >>  ", generation);
        stdout.flush()?;
        if stdin.read_line(&mut buffer)? == 0 {
            println!("All done!");
            return Ok(());
        }
        buffer.pop();
        match buffer.parse::<usize>() {
            Ok(n) => {
                optimizer.parallel_score();
                optimizer.sort();
                println!(
                    "\nGeneration {}: Mean = {}, Standard Deviation = {}, Population = {}\nBest Specimen (Fitness {}):\n{:?}",
                    generation, optimizer.mean_loss(), optimizer.loss_stddev(),
                    optimizer.population(),
                    optimizer.top_fitness().unwrap(),
                    optimizer.top().unwrap()
                );
                optimizer.select(n, &mut rng, learning_rate, change_rate);
                generation += 1;
            },
            Err(_) => match buffer.as_ref() {
                "" => {},
                "show" => println!("{:#?}", optimizer),
                "score" => optimizer.score_all(),
                "generate_all" => {
                    let files = args.to_vec();
                    let mut total_score = 0.0;
                    println!("Generating tracks {:?}", files);
                    let best = optimizer.top().unwrap();
                    for (i, file) in files.iter().enumerate() {
                        let track = Track::from_csv(File::open(file).unwrap());
                        let mvtrack = MaxVelocityTrack{points : track.to_max_vel(&best.car)};
                        let mut simulator = Simulator::new(
                            best.car.clone(), best.params[i].clone(), track.points.len());
                        if simulator.simulate(&mvtrack, &mut rng) {
                            println!("HUNG @{}", i);
                            continue;
                        } else {
                            let time = simulator.get_time();
                            println!("#{} did {}", i, time);
                            total_score += time * weights[i];
                            let mut output_file = File::create(names[i]).unwrap();
                            writeln!(&mut output_file, "a, pit_stop").unwrap();
                            for inst in simulator.inst {
                                writeln!(&mut output_file, "{}, {}", inst.acc, if inst.pit {1} else {0}).unwrap();
                            }
                        }
                    }
                    let mut car = File::create("car.csv").unwrap();
                    writeln!(
                        &mut car, "acc, breaking, speed, gas, tire, handling\n{}, {}, {}, {}, {}, {}",
                        best.car.acceleration as u8 + 1,
                        best.car.breaking as u8 + 1,
                        best.car.speed as u8 + 1,
                        best.car.gas as u8 + 1,
                        best.car.tire as u8 + 1,
                        best.car.handling as u8 + 1
                    ).unwrap();
                    println!("Did {} vs {}", total_score, optimizer.top_fitness().unwrap());
                },
                _ => println!("Invalid command \"{}\"", buffer)
            }
        };
        buffer.clear();
    }
}
