use std::cmp::Ordering;
use rand::Rng;

pub trait Mutateable {
    fn mutate<T: Rng>(&mut self, rng : &mut T, learning_rate : f64, change_chance : f64);
}

pub trait Scorer<T: Mutateable> {
    fn loss(&self, specimen : &T) -> f64;
}

#[derive(Clone, PartialEq, Debug)]
pub struct Scored<T : PartialEq> {
    pub organism : T,
    pub score : Option<f64>
}

impl<T : PartialEq> PartialOrd for Scored<T> {
    fn partial_cmp(&self, other : &Scored<T>) -> Option<Ordering> {
        match (self.score, other.score) {
            (Some(score), Some(other)) => score.partial_cmp(&other),
            _ => None
        }
    }
}

impl<T: Mutateable + PartialEq> Scored<T> {
    pub fn score<S: Scorer<T>>(&mut self, scorer : &S) {
        self.score = Some(scorer.loss(&self.organism));
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Optimizer<T: Mutateable + PartialEq, S : Scorer<T>> {
    organisms : Vec<Scored<T>>,
    scorer : S
}


impl<T: Mutateable + Clone + PartialEq, S : Scorer<T>> Optimizer<T, S> {

    pub fn new(organisms : Vec<T>, scorer : S) -> Optimizer<T, S> {
        Optimizer{
            organisms : organisms.into_iter()
                .map(|o| Scored{organism : o, score : None})
                .collect(),
            scorer : scorer
        }
    }

    pub fn empty(scorer : S) -> Optimizer<T, S> {
        Optimizer{
            organisms : Vec::new(),
            scorer : scorer
        }
    }

    pub fn add_mutated_clones<D: Rng>(
        &mut self, n : usize, specimen : T, rng : &mut D,
        learning_rate : f64, change_rate : f64) {
        self.organisms.reserve(n);
        for _ in 0..n {
            let mut clone = specimen.clone();
            clone.mutate(rng, learning_rate, change_rate);
            self.organisms.push(Scored{organism : clone, score : None});
        }
    }

    pub fn score_all(&mut self) {
        for organism in &mut self.organisms {organism.score(&self.scorer)}
    }

    fn score_slice(slice : &mut[Scored<T>], scorer : &S) {
        for organism in slice {organism.score(scorer)}
    }

    pub fn sort(&mut self) {
        self.organisms.sort_by(|a, b| a.partial_cmp(b).unwrap())
    }

    pub fn select<D: Rng>(&mut self, n : usize, rng : &mut D,
        learning_rate : f64, change_rate : f64) {
        self.organisms.truncate(n);
        let n = self.organisms.len();
        for i in 0..n {
            self.add_mutated_clones(
                i + 1, self.organisms[i].organism.clone(), rng, learning_rate, change_rate
            );
        }
    }

    pub fn top(&self) -> Option<&T> {
        match self.organisms.first() {
            Some(scored) => Some(&scored.organism),
            None => None
        }
    }

    pub fn mean_loss(&self) -> f64 {
        stats::mean(self.organisms.iter().map(|o| o.score.unwrap()))
    }

    pub fn loss_stddev(&self) -> f64 {
        stats::stddev(self.organisms.iter().map(|o| o.score.unwrap()))
    }

    pub fn population(&self) -> usize {self.organisms.len()}

    pub fn top_fitness(&self) -> Option<f64> {
        match self.organisms.first() {
            Some(scored) => scored.score,
            None => None
        }
    }

}

impl<T: Mutateable + Clone + PartialEq + Send, S : Scorer<T> + Clone + Send> Optimizer<T, S> {
    pub fn parallel_score(&mut self) {
        let num_threads = rayon::current_num_threads();
        let chunk_size =
            (self.organisms.len() + num_threads - 1)/num_threads;
        rayon::scope(|scope| {
            for chunk in self.organisms.chunks_mut(chunk_size as usize) {
                let thread_scorer = self.scorer.clone();
                scope.spawn(move |_| {
                    Self::score_slice(chunk, &thread_scorer);
                });
            }
        })
    }
}
