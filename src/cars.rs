use crate::optimizer::{Mutateable};
use rand::{Rng};

#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub enum Tier {
    Tier1, Tier2, Tier3, Tier4, Tier5
}

impl Tier {
    pub fn points(&self) -> u32 {
        match self {
            Tier::Tier1 => 0,
            Tier::Tier2 => 2,
            Tier::Tier3 => 3,
            Tier::Tier4 => 4,
            Tier::Tier5 => 6
        }
    }
    pub fn reduce(&mut self) -> u32 {
        match self {
            Tier::Tier1 => 0,
            Tier::Tier2 => {*self = Tier::Tier1; 2},
            Tier::Tier3 => {*self = Tier::Tier2; 1},
            Tier::Tier4 => {*self = Tier::Tier3; 1},
            Tier::Tier5 => {*self = Tier::Tier4; 2}
        }
    }
    pub fn increase(&mut self) -> u32 {
        match self {
            Tier::Tier1 => {*self = Tier::Tier2; 2},
            Tier::Tier2 => {*self = Tier::Tier3; 1},
            Tier::Tier3 => {*self = Tier::Tier4; 1},
            Tier::Tier4 => {*self = Tier::Tier5; 2}
            Tier::Tier5 => {0}
        }
    }
}

pub fn tier(n : u32) -> Tier {
    match n {
        1 => Tier::Tier1,
        2 => Tier::Tier2,
        3 => Tier::Tier3,
        4 => Tier::Tier4,
        5 => Tier::Tier5,
        _ => panic!("Invalid tier!")
    }
}

#[derive(Eq, PartialEq, Debug, Clone, Copy)]
pub struct Car {
    pub tire : Tier,
    pub gas : Tier,
    pub handling : Tier,
    pub speed : Tier,
    pub acceleration : Tier,
    pub breaking : Tier
}

impl Car {
    pub fn new_random<T: Rng>(limit : u32, rng : &mut T) -> Car {
        let mut result = Car {
            tire : Tier::Tier1,
            gas : Tier::Tier1,
            handling : Tier::Tier1,
            speed : Tier::Tier1,
            acceleration : Tier::Tier1,
            breaking : Tier::Tier1,
        };
        while result.increasable(limit) {result.random_increase(limit, rng);}
        result
    }
    pub fn points(&self) -> u32 {
        self.tire.points() +
        self.gas.points() +
        self.handling.points() +
        self.speed.points() +
        self.acceleration.points() +
        self.breaking.points()
    }
    pub fn increasable(&self, limit : u32) -> bool {
        let limit = std::cmp::min(limit, 5 * 6); // Best possible car
        let points = self.points();
        if points >= limit {
            false
        } else if points >= limit - 1 {
            for i in 0..6 {
                match self.get(i) {
                    Tier::Tier2 | Tier::Tier3 => {return true;}
                    _ => {}
                }
            }
            false
        } else {
            true
        }
    }
    pub fn get(&self, i : usize) -> Tier {
        match i {
            0 => self.tire,
            1 => self.gas,
            2 => self.handling,
            3 => self.speed,
            4 => self.acceleration,
            5 => self.breaking,
            _ => panic!("Out of range!")
        }
    }
    pub fn get_rand<T: Rng>(&self, rng : &mut T) -> Tier {self.get(rng.gen_range(0, 6))}
    pub fn get_mut(&mut self, i : usize) -> &mut Tier {
        match i {
            0 => &mut self.tire,
            1 => &mut self.gas,
            2 => &mut self.handling,
            3 => &mut self.speed,
            4 => &mut self.acceleration,
            5 => &mut self.breaking,
            _ => panic!("Out of range!")
        }
    }
    pub fn get_rand_mut<T: Rng>(&mut self, rng : &mut T) -> &mut Tier {
        self.get_mut(rng.gen_range(0, 6))}
    pub fn random_increase<T: Rng>(&mut self, limit : u32, rng : &mut T) {
        let rn = rng.gen_range(0, 6);
        self.get_mut(rn).increase();
        if self.points() > limit {self.get_mut(rn).reduce();}
    }
    pub fn top_speed(&self) -> f64 {
        match self.speed {
            Tier::Tier1 => 10.0,
            Tier::Tier2 => 20.0,
            Tier::Tier3 => 30.0,
            Tier::Tier4 => 40.0,
            Tier::Tier5 => 50.0
        }
    }
    pub fn handling_coefficient(&self) -> f64 {
        match self.handling {
            Tier::Tier1 => 9.0,
            Tier::Tier2 => 12.0,
            Tier::Tier3 => 15.0,
            Tier::Tier4 => 18.0,
            Tier::Tier5 => 21.0
        }
    }
    pub fn top_speed_rad(&self, rad : Option<f64>) -> f64 {
        match rad {
            Some(rad) => (rad * self.handling_coefficient() / 1000000.0)
                .sqrt().min(self.top_speed()),
            None => self.top_speed()
        }
    }
    pub fn acc_between(v1 : f64, v2 : f64) -> f64 {(v2*v2 - v1*v1) / 2.0}
    pub fn get_acc_nv(v : f64, a : f64) -> f64 {(v*v + 2.0*a).sqrt()}
    pub fn max_gas(&self) -> f64 {(self.gas as u8) as f64 * 250.0 + 500.0}
    pub fn max_tire(&self) -> f64 {(self.tire as u8) as f64 * 250.0 + 500.0}
    pub fn max_break(&self) -> f64 {(self.breaking as u8) as f64 * -5.0 - 10.0}
    pub fn max_acc(&self) -> f64 {(self.acceleration as u8) as f64 * 5.0 + 10.0}
}

impl Mutateable for Car {

    fn mutate<T: Rng>(&mut self, rng : &mut T, _learning_rate : f64, change_chance : f64) {
        if rng.gen_bool(change_chance) {
            if self.get_rand_mut(rng).reduce() != 0 {}
            while self.increasable(18) {
                self.random_increase(18, rng)
            }
        }
    }
}

/*This is the simple pathfinder proposed by Aman.*/
/*
How it works:
- At each time step, algorithm computes/observes corresponding maximum velocity values for
    each time step given the radius of curvature at that time step.
- It then will then linearly interpolate (with respect to distance) between the current velocity
    and the velocity in the future with the minimum (delta velocity/delta distance) value.
- Algorithm for PITSTOPS:
    Case 1: The pitstop is required because of no gas.
        Wait until acceleration is in order, then take a pitstop immediately before that.
    Case 2: Breaks fail.
        Look back 'b' (=50) steps. See where velocity is lowest and go to that place. Decelerate as
        fast as possible given the amount of breaks.
            How to decelerate?


*/
#[derive(PartialEq, Clone, Copy, Debug)]
pub struct SimplePathParameters {
    pub n : u32, //lookahead param
    pub min_n : u32,
    pub m : u32, // increasing n param
    pub a : u32, // decreasing n param
    pub pa : f64 // a_chance
}

impl SimplePathParameters {
    pub fn default() -> SimplePathParameters {SimplePathParameters{
        n : 4,
        min_n : 2,
        m : 5,
        a : 1,
        pa : 0.05
    }}
}

impl Mutateable for SimplePathParameters {
    fn mutate<T: Rng>(&mut self, rng : &mut T, learning_rate : f64, change_rate : f64) {
        // Change n and/or min_n
        if rng.gen_bool(change_rate) {
            if rng.gen_bool(1.0 / 3.0) {
                self.n = (self.n - 1).max(self.min_n);
                self.min_n = (self.min_n - 1).max(2);
            } else if rng.gen_bool(1.0 / 2.0) {
                self.n = (self.n - 1).max(self.min_n);
            } else {
                self.min_n = (self.min_n - 1).max(2);
            }
        }
        if rng.gen_bool(change_rate) {
            self.m = (self.m - 1).max(1);
        }
        if rng.gen_bool(change_rate) {
            self.a = (self.a - 1).max(1);
        }
        let dist = rand::distributions::Normal::new(0.0, learning_rate / 5.0);
        self.pa += rng.sample(dist);
        self.pa = self.pa.max(0.001).min(1.0);
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct CarSimulationParams {
    pub car : Car,
    pub params : Vec<SimplePathParameters>
}

impl Mutateable for CarSimulationParams {
    fn mutate<T: Rng>(&mut self, rng : &mut T, learning_rate : f64, change_rate : f64) {
        self.car.mutate(rng, learning_rate, change_rate);
        for params in &mut self.params {
            params.mutate(rng, learning_rate, change_rate);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn random_cars_have_almost_full_points() {
        let mut rng = rand::thread_rng();
        for i in 1..30 {
            for _ in 0..5 {
                let car = Car::new_random(i, &mut rng);
                assert!(car.points() >= i - 1);
                assert!(car.points() <= i);
            }
        }
    }

    #[test]
    fn maximum_speeds_are_computed_properly() {
        let test_car = Car{
            tire : tier(3),
            gas : tier(3),
            handling : tier(3),
            speed : tier(3),
            acceleration : tier(3),
            breaking : tier(3)
        };
        assert_eq!(test_car.top_speed(), 30.0);
        assert_eq!(test_car.top_speed_rad(None), 30.0);
        assert_eq!(test_car.top_speed_rad(Some(15000000.0)), 15.0);
    }
}
