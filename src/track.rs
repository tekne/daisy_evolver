use std::iter::FromIterator;
use crate::cars::{Car};
use std::fs::File;

#[derive(PartialEq, Clone, Debug)]
pub struct Track{
    pub points: Vec<Option<f64>>
}

#[derive(Debug, Deserialize, Clone, Copy)]
pub struct TrackRecord {
    radius : f64
}

impl Track {
    pub fn new<T: Iterator<Item=Option<f64>>>(points : T) -> Track{
        Track{points : Vec::from_iter(points)}
    }
    pub fn new_positive<T: Iterator<Item=f64>>(points : T) -> Track {
        Track{points : points.map(|p| if p >= 0.0 {Some(p)} else {None}).collect()}
    }
    pub fn len(&self) -> usize {self.points.len()}
    pub fn to_max_vel<B: FromIterator<f64>>(&self, c : &Car) -> B {
        self.points.iter().map(|p| c.top_speed_rad(*p)).collect()
    }

    pub fn from_csv(file : File) -> Track {
        let mut rdr = csv::Reader::from_reader(file);
        Track::new(rdr.deserialize::<TrackRecord>()
            .map(|tr| {
                let r = tr.unwrap().radius;
                if r < 0.0 {None} else {Some(r)}
            }))
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct MaxVelocityTrack{
    pub points: Vec<f64>
}

pub fn to_max_acc(v : &[f64]) -> std::iter::Map<std::slice::Windows<f64>, &Fn(&[f64]) -> f64> {
    v.windows(2).map(&|w : &[f64]| {Car::acc_between(w[0], w[1])})
}

pub fn to_times(v : &[f64]) -> std::iter::Map<
    std::iter::Zip<
        std::slice::Windows<f64>,
        std::iter::Map<std::slice::Windows<f64>, &Fn(&[f64]) -> f64>>,
    &Fn((&[f64], f64)) -> f64
> {
    let acc = to_max_acc(v);
    v.windows(2).zip(acc).map(&|w : (&[f64], f64)| -> f64 {
        let (slice, acc) = w;
        if acc == 0.0 {1.0/slice[0]} else {(slice[1] - slice[0])/acc}
    })
}

impl MaxVelocityTrack {
    pub fn new<T: Iterator<Item=f64>>(points : T)
        -> MaxVelocityTrack {MaxVelocityTrack{points : Vec::from_iter(points)}}
    pub fn adjust(&mut self) {
        if self.points.len() <= 1 {return}
        for i in 1..self.points.len() {
            let min =
                self.points[self.points.len() - i].min(self.points[self.points.len() - i - 1]);
            let len = self.points.len();
            self.points[len - i] = min;
        }
    }
    pub fn to_max_acc<B : FromIterator<f64>>(&self) -> B {
        to_max_acc(&self.points[..]).collect()
    }
}
