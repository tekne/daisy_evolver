use rand::Rng;
use rand::distributions::{Normal, Distribution};
use crate::optimizer::{Mutateable, Scorer, Optimizer};

use std::io;
use std::io::Write;

#[derive(PartialEq, Clone, Debug)]
struct SquareSum {
    elements : Vec<f64>
}

impl SquareSum {
    pub fn new_random<T: Rng, D: Distribution<f64>>(n : usize, rng : &mut T, dist : &D)
    -> SquareSum {
        let mut result = Vec::with_capacity(n);
        for _ in 0..n {result.push(dist.sample(rng));}
        SquareSum{elements : result}
    }
}

impl Mutateable for SquareSum {
    fn mutate<T : Rng>(&mut self, rng : &mut T, learning_rate : f64, change_rate : f64) {
        let normal = Normal::new(0.0, learning_rate);
        for element in &mut self.elements {
            *element += normal.sample(rng);
        }
        if self.elements.len() < 10 && rng.gen_bool(
            change_rate * (self.elements.len() as f64 + 1.0) / 11.0) {
            self.elements.push(*self.elements.last().unwrap())
        } else if self.elements.len() > 1 && rng.gen_bool(
            change_rate * (self.elements.len() as f64 - 1.0) / 30.0
        ) {
            self.elements.pop();
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct SquareSumScorer;

impl Scorer<SquareSum> for SquareSumScorer {
    fn loss(&self, specimen : &SquareSum) -> f64 {
        let mut rng_instance = rand::thread_rng();
        let product = specimen.elements.iter().product::<f64>();
        let sum_of_squares = specimen.elements.iter().map(|e| e * e).sum::<f64>();
        for _ in 0..10000 {
            if rng_instance.gen_bool(0.00001) {return -100.0;}
        }
        product / sum_of_squares
    }
}

pub fn squaresum() -> io::Result<()> {
    let starting_dist = Normal::new(5.0, 0.5);
    let learning_rate = 0.05;
    let change_rate = 0.05;
    let mut rng = rand::thread_rng();
    let squaresum = SquareSum::new_random(3, &mut rng, &starting_dist);
    let mut optimizer = Optimizer::empty(SquareSumScorer{});
    optimizer.add_mutated_clones(4, squaresum, &mut rng, learning_rate, change_rate);

    let stdin = io::stdin();
    let mut stdout = io::stdout();
    let mut buffer = String::new();
    let mut generation = 0;
    loop {
        print!("{} >>  ", generation);
        stdout.flush()?;
        if stdin.read_line(&mut buffer)? == 0 {
            println!("All done!");
            return Ok(());
        }
        buffer.pop();
        match buffer.parse::<usize>() {
            Ok(n) => {
                optimizer.parallel_score();
                optimizer.sort();
                println!(
                    "Generation {}: Mean = {}, Standard Deviation = {}, Population = {}\nBest Specimen = {:?}",
                    generation, optimizer.mean_loss(), optimizer.loss_stddev(),
                    optimizer.population(),
                    optimizer.top().unwrap()
                );
                optimizer.select(n, &mut rng, learning_rate, change_rate);
                generation += 1;
            },
            Err(_) => match buffer.as_ref() {
                "" => {},
                "show" => println!("{:#?}", optimizer),
                "score" => optimizer.score_all(),
                _ => println!("Invalid command \"{}\"", buffer)
            }
        };
        buffer.clear();
    }
}
